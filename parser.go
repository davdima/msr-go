package main

import (
	"encoding/json"
	"fmt"
	"./taobao"
	"log"
	"net/http"
	"net/url"
	"regexp"
)

var parseUrlRexep = regexp.MustCompile("url=(.*?)$")

type ErrorResponse struct {
	Message string `json:"message"`
}

func Log(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("%s %s %s", r.RemoteAddr, r.Method, r.URL)
		handler.ServeHTTP(w, r)
	})
}

func main() {

	fmt.Println("Starting")

	//	product := taobao.NewProduct("http://tw.taobao.com/item/37786256841.htm?spm=a1z3o.7406521.0.0.12P7VZ")
	//	product := taobao.NewProduct("http://item.taobao.com/item.htm?spm=2013.1.20141003.7.M3ISal&scm=1007.10011.1558.0&id=37235494780&pvid=9a27ea0a-6f97-497b-8b9d-9c30a0e43efe")
	//	err := product.Parse()
	//	if err != nil {
	//		panic(err)
	//	}
	//	j, _ := json.Marshal(product)
	//	fmt.Printf("%#v", string(j))

	http.HandleFunc("/product/", func(w http.ResponseWriter, r *http.Request) {
			inputUrl := parseUrlRexep.FindStringSubmatch(r.URL.String())

			w.Header().Set("Content-Type", "application/json")

			encoder := json.NewEncoder(w)
			defer func() {
				if r := recover(); r != nil {
					w.WriteHeader(http.StatusBadRequest)
					encoder.Encode(&ErrorResponse{fmt.Sprintf("%s", r)})
				}
			}()

			if len(inputUrl) == 0 {
				panic("URL missmatch")
			}

			_, err := url.Parse(inputUrl[1])

			if err != nil {
				panic(fmt.Sprintf("%s", err))
			}

			product := taobao.NewProduct(inputUrl[1])
			if err := product.Parse(); err != nil {
				panic(fmt.Sprintf("%s", err))
			}

			encoder.Encode(product)
		})

	log.Fatal(http.ListenAndServe(":8181", Log(http.DefaultServeMux)))
}
