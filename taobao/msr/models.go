package msr

type Product struct {
	Id              uint32 `db:"id"`
	CategoryId      uint32 `db:"category_id"`
	CategoryAlbumId uint32 `db:"category_album_id"`
	CategoryJobId   uint32 `db:"category_job_id"`
	ProductTypeId   uint32 `db:"product_type_id"`
	Pid             uint32 `db:"pid"`
	PidFrom         uint32 `db:"pid_from"`
	ProductId       uint64 `db:"product_id"`
	InStock         uint8 `db:"in_stock"`
	ImageId         uint32 `db:"image_id"`
}
