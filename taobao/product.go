package taobao

import (
	"bytes"
	"github.com/PuerkitoBio/goquery"
	iconv "github.com/djimenez/iconv-go"
	"io/ioutil"
	"net/http"
	"regexp"
	"./parser"
)

var (
	domainCheckTaobao = regexp.MustCompile("item\\.taobao")
	domainCheckTw     = regexp.MustCompile("tw\\.taobao\\.com")
	domainCheckTmall  = regexp.MustCompile("tmall\\.com")
)

type Product struct {
	Url         string     `json:"url"`
	ProductId   string     `json:"product_id"`
	Sku         parser.Sku `json:"sku"`
	Images      []string   `json:"images"`
	IsAvailable bool       `json:"is_available"`
	Seller      string     `json:"seller"`

	parser parser.Parser
}

func NewProduct(rawurl string) *Product {

	product := &Product{}
	resp, err := http.Get(rawurl)

	if err != nil {
		panic(err)
		product.IsAvailable = false
		return product
	}
	defer resp.Body.Close()

	in, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
		product.IsAvailable = false
		return product
	}

	out := make([]byte, len(in)*2)
	_, _, err = iconv.Convert(in, out, "gbk", "utf-8//IGNORE")
	if err != nil {
		panic(err)
		product.IsAvailable = false
		return product
	}

	doc, err := goquery.NewDocumentFromReader(bytes.NewBuffer(out))
	if err != nil {
		panic(err)
		product.IsAvailable = false
		return product
	}

	finalUrl := resp.Request.URL.String()

	switch {
	case domainCheckTaobao.MatchString(finalUrl):
		product.parser = parser.NewTaobao(doc)
	case domainCheckTmall.MatchString(finalUrl):
		product.parser = parser.NewTmall(doc)
	case domainCheckTw.MatchString(finalUrl):
		product.parser = parser.NewTw(doc)
	}

	productId, productUrl, err := product.parser.ParseAndNormalizeUrl(finalUrl)
	product.Url = productUrl
	product.ProductId = productId

	return product
}


func (p *Product) Parse() error {
	p.IsAvailable = p.parser.IsInStock()
	p.Sku = p.parser.GetSkuMap()
	p.Seller = p.parser.GetSellerName()
	p.Images = p.parser.GetProductImages()

	return nil
}
