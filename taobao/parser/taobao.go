package parser

import (
	"github.com/PuerkitoBio/goquery"
	"encoding/json"
	"regexp"
	"strings"

	"strconv"
)

var taobaoSkuRegExp = regexp.MustCompile("\"skuMap\"\\:(.*?)}}\\);");

func NewTaobao(document *goquery.Document) *Taobao {
	return &Taobao{document: document}
}

type Taobao struct {
	document *goquery.Document
}

func (t Taobao) GetSkuMap() Sku {
	//J_Prop
	html, err := t.document.Html()
	skuStruct := newSku()
	if (err == nil) {
		html = strings.Replace(html, "\n", "", -1)
		html = strings.Replace(html, "\t", "", -1)
		finded := taobaoSkuRegExp.FindStringSubmatch(html)

		var skuMap map[string]*SkuItem

		if len(finded) > 1 {
			json.Unmarshal([]byte(finded[1]), &skuMap)
			for key, skuItem := range skuMap {
				cent, _ := strconv.ParseFloat(skuItem.Price, 64)
				skuMap[key].PriceCent = uint(cent * 100)
			}
			skuStruct.Map = skuMap
		}

		t.document.Find(".J_Prop").Find("ul").Each(func (i int, ul *goquery.Selection) {
			if dataProperty, ok := ul.Attr("data-property"); ok {

				ul.Find("li").Each(func(n int, li *goquery.Selection){
					properyItemText := strings.TrimSpace(li.Text())

					skuPropertyInstance := &SkuProperty{Text : properyItemText}

					a := li.Find("a").First();
					if style, ok := a.Attr("style"); ok {
						if background, err := getImageUrlFromBackgroundStyle(style); err != nil {
							skuPropertyInstance.Image = imageNormalize(background)
						}
					}

					skuStruct.addProperty(dataProperty, *skuPropertyInstance)
				})
			}
		})
	}

	return *skuStruct
}
func (t Taobao) GetProductImages() []string {
	var images []string
	t.document.Find("#J_UlThumb").Find("img").Each(func(i int, img *goquery.Selection) {
		if src, ok := img.Attr("data-src"); ok {
			images = append(images, imageNormalize(src))
		}
	})
	return images
}

func (t Taobao) GetSellerName() string {
	sellerName, _ := t.document.Find("#J_ShopInfo").Find(".tb-shop-name").Find("a").Attr("title")
	return sellerName;
}

func (t Taobao) IsInStock() bool {
	isOffSale := t.document.Find(".tb-key-off-sale").Length() == 0
	isOutOfSell := t.document.Find(".tb-out-of-date").Length() == 0
	return isOffSale && isOutOfSell
}

func (t Taobao) IsNotFound() bool {
	return t.document.Find("#error-notice").Length() != 0
}

func (t Taobao) CheckAvailable() bool {
	return true
}

func (t Taobao) ParseAndNormalizeUrl(rawUlr string) (string, string, error) {
	return NormalizeUrl(rawUlr)
}
