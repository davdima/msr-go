package parser

import (
	"github.com/PuerkitoBio/goquery"
	"encoding/json"
	"strings"
	"net/url"
	"regexp"
	"errors"
)

var (
	regexpTwProductId = regexp.MustCompile("\\/item\\/([0-9]+).htm")
	twSkuRegExp = regexp.MustCompile("skuMap\\:(.*?)}}")
	showNameRegExp = regexp.MustCompile("shopTitle:\"(.*?)\"")
)

func NewTw(document *goquery.Document) *Tw {
	return &Tw{document: document}
}

type Tw struct {
	document *goquery.Document
}

func (t Tw) GetSkuMap() Sku {
	html, err := t.document.Html()
	skuStruct := newSku()
	if (err == nil) {
		html = strings.Replace(html, "\n", "", -1)
		html = strings.Replace(html, "\t", "", -1)
		finded := twSkuRegExp.FindStringSubmatch(html)

		if len(finded) > 1 {
			err := json.Unmarshal([]byte(finded[1] + "}}"), &skuStruct.Map)
			if err != nil {
				panic(err)
			}
		}
//		panic(fmt.Sprintf("%#v",  &skuStruct.Map))

		t.document.Find(".item-sku").Find("dl").Each(func (i int, dl *goquery.Selection) {

			dataProperty := dl.Find("dt").Text()

			dl.Find("li").Each(func(n int, li *goquery.Selection) {

				properyItemText := strings.TrimSpace(li.Text())

				skuPropertyInstance := &SkuProperty{Text : properyItemText}

				a := li.Find("a").First();
				if style, ok := a.Attr("style"); ok {
					if background, err := getImageUrlFromBackgroundStyle(style); err == nil {
						skuPropertyInstance.Image = imageNormalize(background)
					}
				}

				skuStruct.addProperty(dataProperty, *skuPropertyInstance)
			})

		})
	}
	return *skuStruct
}

func (t Tw) GetSellerName() string {

	html, err := t.document.Html()
	if err != nil {
		return ""
	}

	regexpResult := showNameRegExp.FindStringSubmatch(html)
	if len(regexpResult) > 0 {
		return strings.TrimSpace(regexpResult[1])
	}

	return ""
}

func (t Tw) GetProductImages() []string {
	var productImages []string
	t.document.Find("#J_ThumbNav").Find("img").Each(func(i int, s *goquery.Selection) {
		if attr, ok := s.Attr("src"); ok {
			productImages = append(productImages, imageNormalize(attr))
		}
	})

	return productImages
}

func (t Tw) IsInStock() bool {
	return t.document.Find(".warning-info").Length() == 0
}

func (t Tw) IsNotFound() bool {
	return true
}

func (t Tw) CheckAvailable() bool {
	return true
}

func (t Tw) ParseAndNormalizeUrl(rawurl string) (string, string, error) {
	parsedUrl, err := url.Parse(rawurl)
	if err != nil {
		return "", "", err
	}

	query := parsedUrl.Query()
	for key, _ := range query {
		delete(query, key)
	}
	parsedUrl.RawQuery = query.Encode()
	regexpResult := regexpTwProductId.FindStringSubmatch(parsedUrl.String())
	if len(regexpResult) == 0 {
		return "", "", errors.New("Not valid")
	}

	return regexpResult[1], parsedUrl.String(), nil
}
