package parser

import (
	"github.com/PuerkitoBio/goquery"
	"regexp"
	"encoding/json"
	"strings"
)

var parseSkuRexExp = regexp.MustCompile("\"skuMap\":(.*?)},\"valLoginIndicator\"")

func NewTmall(document *goquery.Document) *Tmall {
	return &Tmall{document: document}
}

type Tmall struct {
	document *goquery.Document
}

func (t Tmall) GetSkuMap() Sku {
	html, err := t.document.Html()
	skuStruct := newSku()
	if (err == nil) {

		finded := parseSkuRexExp.FindStringSubmatch(html)

		if len(finded) > 1 {
			json.Unmarshal([]byte(finded[1]), &skuStruct.Map)
		}

		t.document.Find(".tb-sku").Find("ul").Each(func (i int, ul *goquery.Selection) {
			if dataProperty, ok := ul.Attr("data-property"); ok {

				ul.Find("li").Each(func(n int, li *goquery.Selection){
					properyItemText := strings.TrimSpace(li.Text())

					skuPropertyInstance := &SkuProperty{Text : properyItemText}

					a := li.Find("a").First();
					if style, ok := a.Attr("style"); ok {
						if background, err := getImageUrlFromBackgroundStyle(style); err != nil {
							skuPropertyInstance.Image = imageNormalize(background)
						}
					}

					skuStruct.addProperty(dataProperty, *skuPropertyInstance)
				})
			}
		})
	}
	return *skuStruct
}

func (t Tmall) GetSellerName() string {
	showName, _ := t.document.Find("[name=\"seller_nickname\"]").Attr("value")
	return strings.TrimSpace(showName)
}

func (t Tmall) GetProductImages() []string {
	var productImages []string
	t.document.Find("#J_UlThumb").Find("img").Each(func(i int, s *goquery.Selection) {
		if attr, ok := s.Attr("src"); ok {
			productImages = append(productImages, imageNormalize(attr))
		}
	})

	return productImages
}

func (t Tmall) IsInStock() bool {
	return true
}

func (t Tmall) IsNotFound() bool {
	return true
}


func (t Tmall) CheckAvailable() bool {
	return true
}

func (t Tmall) ParseAndNormalizeUrl(rawurl string) (string, string, error) {
	return NormalizeUrl(rawurl)
}
