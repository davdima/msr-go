package parser

import (
	"github.com/PuerkitoBio/goquery"
	"encoding/json"
	"strings"
	"net/url"
	"fmt"
)

func NewTaiwan(document *goquery.Document) *Taiwan {
	return &Taiwan{document: document}
}

type Taiwan struct {
	document *goquery.Document
}

func (t Taiwan) GetSkuMap() Sku {
	html, err := t.document.Html()
	skuStruct := newSku()
	if (err == nil) {

		finded := parseSkuRexExp.FindStringSubmatch(html)

		if len(finded) > 1 {
			json.Unmarshal([]byte(finded[1]), &skuStruct.Map)
		}

		t.document.Find(".tb-sku").Find("ul").Each(func (i int, ul *goquery.Selection) {
			if dataProperty, ok := ul.Attr("data-property"); ok {

				ul.Find("li").Each(func(n int, li *goquery.Selection){
					properyItemText := strings.TrimSpace(li.Text())

					skuPropertyInstance := &SkuProperty{Text : properyItemText}

					a := li.Find("a").First();
					if style, ok := a.Attr("style"); ok {
						if background, err := getImageUrlFromBackgroundStyle(style); err != nil {
							skuPropertyInstance.Image = imageNormalize(background)
						}
					}

					skuStruct.addProperty(dataProperty, *skuPropertyInstance)
				})
			}
		})
	}
	return *skuStruct
}

func (t Taiwan) GetSellerName() string {
	showName, _ := t.document.Find("[name=\"seller_nickname\"]").Attr("value")
	return strings.TrimSpace(showName)
}

func (t Taiwan) GetProductImages() []string {
	var productImages []string
	t.document.Find("#J_UlThumb").Find("img").Each(func(i int, s *goquery.Selection) {
		if attr, ok := s.Attr("src"); ok {
			productImages = append(productImages, imageNormalize(attr))
		}
	})

	return productImages
}

func (t Taiwan) IsInStock() bool {
	return true
}

func (t Taiwan) IsNotFound() bool {
	return true
}

func (t Taiwan) CheckAvailable() bool {
	return true
}

func (t Taiwan) ParseAndNormalizeUrl(rawUrl string) (string, string, error) {
	parsedUrl, err := url.Parse(rawUrl)
	if err != nil {
		return "", "", err
	}
	fmt.Printf("%#v", parsedUrl)
	return "", "", nil
}
