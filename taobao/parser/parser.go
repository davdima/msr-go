package parser

import (
	"errors"
	"net/url"
	"regexp"
)

var imageNormalizeRegExp = regexp.MustCompile("_[0-9]{1,}x{1,}.*?$")
var backgroundUrlRegExp = regexp.MustCompile("background:url\\((.*?)\\)")

type Parser interface {
	GetSkuMap() Sku
	GetSellerName() string
	GetProductImages() []string
	IsInStock() bool
	IsNotFound() bool
	ParseAndNormalizeUrl(url string) (string, string, error)
}

type Sku struct {
	Map        map[string]*SkuItem      `json:"map"`
	Properties map[string][]SkuProperty `json:"properties"`
}

type SkuProperty struct {
	Image string `json:"image"`
	Text  string `json:"text"`
}

func (s *Sku) addProperty(propertyName string, skyPropertyItems SkuProperty) {
	s.Properties[propertyName] = append(s.Properties[propertyName], skyPropertyItems)
}

func newSku() *Sku {
	return &Sku{
		Map:        make(map[string]*SkuItem),
		Properties: make(map[string][]SkuProperty),
	}
}

type SkuItem struct {
	Price     string `json:"price"`
	PriceCent uint   `json:"priceCent"`
	SkuId     string `json:"skuId"`
	Stock     string `json:"stock"`
}

func imageNormalize(image string) string {
	return imageNormalizeRegExp.ReplaceAllLiteralString(image, "")
}

func getImageUrlFromBackgroundStyle(style string) (string, error) {
	background := backgroundUrlRegExp.FindStringSubmatch(style)
	if len(background) > 1 {
		return background[1], nil
	}

	return "", errors.New("Image not found in background style property")
}

func NormalizeUrl(rawUrl string) (string, string, error) {
	parsedUrl, err := url.Parse(rawUrl)
	if err != nil {
		return "", "", errors.New("Url not valid")
	}

	query := parsedUrl.Query()
	if err != nil {
		return "", "", errors.New("Url not valid")
	}

	productId := query.Get("id")

	if productId == "" {
		return "", "", errors.New("Url not valid")
	}

	for key, _ := range query {
		if key != "id" {
			delete(query, key)
		}
	}
	parsedUrl.RawQuery = query.Encode()
	return productId, parsedUrl.String(), nil
}
